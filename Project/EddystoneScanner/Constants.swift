//
//  Constants.swift
//  EddystoneScanner
//
//  Created by Shrikant Soni on 26/06/18.
//  Copyright © 2018 Shrikant Soni. All rights reserved.
//

import Foundation

///
/// Constants
///
/// Keeps track of all the constants used in the project
///
internal class Constants {
    static let BEACON_OPERATION_QUEUE_LABEL = "com.eddystonescanner.queue.blescanner"
    static let DISPATCH_TIMER_QUEUE_LABEL = "com.eddystonescanner.queue.dispatchtimer"
}
