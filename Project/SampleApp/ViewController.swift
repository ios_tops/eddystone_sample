//
//  ViewController.swift
//  SampleApp
//
//  Created by Shrikant Soni on 25/06/18.
//  Copyright © 2017 Shrikant Soni. All rights reserved.
//

import UIKit
import EddystoneScanner
import UserNotifications
import SafariServices
class ViewController: UIViewController {
    
    let scanner = EddystoneScanner.Scanner()
    var timer : Timer?
    
    var beaconList = [Beacon]()

    @IBOutlet weak var tableView: UITableView!
    var strURL:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        scanner.startScanning()
        scanner.delegate = self
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        strURL = ""
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTableView), userInfo: nil, repeats: true)
    }
    
    @objc func updateTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        timer?.invalidate()
        timer = nil
    }


}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let beacon = beaconList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: BeaconTableViewCell.cellIdentifier) as! BeaconTableViewCell
        cell.configureCell(for: beacon, addURL: strURL)
        return cell
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beaconList.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if strURL.count > 0  {
            let svc = SFSafariViewController(url: URL(string: strURL)!)
            present(svc, animated: true, completion: nil)
        }
    }
}

extension ViewController: ScannerDelegate {
    // MARK: EddystoneScannerDelegate callbacks
    // For find the nearby Beacon
    func didFindBeacon(scanner: EddystoneScanner.Scanner, beacon: Beacon) {
        beaconList.append(beacon)
        updateTableView()
    }
    // For check, if the device is out of range/lost.
    func didLoseBeacon(scanner: EddystoneScanner.Scanner, beacon: Beacon) {
        guard let index = beaconList.index(of: beacon) else {
            return
        }
        beaconList.remove(at: index)
        updateTableView()
    }
    // For data update of Beacon
    func didUpdateBeacon(scanner: EddystoneScanner.Scanner, beacon: Beacon) {
        guard let index = beaconList.index(of: beacon) else {
            beaconList.append(beacon)
            updateTableView()
            return
        }
        beaconList[index] = beacon
    }
    // To find Beacon's advertisement URL.
    func didFindBeaconURL(scanner: EddystoneScanner.Scanner, addURL: URL?) {
        strURL = (addURL?.absoluteString)!
    }
}
